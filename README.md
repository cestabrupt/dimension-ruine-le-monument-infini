# ~/ABRÜPT/DIMENSION RUINE/LE MONUMENT INFINI/*

La [page de ce livre](https://abrupt.ch/dimension-ruine/le-monument-infini/) sur le réseau.

## Sur le livre

Cet essai n’est pas un poème, il n’a rien non plus du songe, il se place en l’œil qui le lit comme une prière chamanique où retentissent des abstractions, s’enchevêtrent des concepts en un tournoiement qu’il veut sans fin. Le tutoiement distant mais aimant de ce texte à la science-fiction accompagne un appel à la transformation de la vision portée sur le réel. Il invite à la réflexion sur l’acte de bâtir, non dans sa technicité mais dans son essence mystique. Le geste qui martèle la pierre pour construire le foyer a pour le collectif Dimension Ruine l’allure d’un mantra politique. Dans cette remise en cause de ce qui nous semble anodin, la constance d’une verticale et d’une horizontale, de l’angle droit qui les réunit, se trouve un aveu de défaite des combats sociaux, un renoncement à la modernité, et dans la contemplation des gravats ontologiques d’une époque, Dimension Ruine murmure une proposition concrète : un monument sans fin. Ce bâtiment semble vouloir conquérir l’espace, il a l’apparence de la prospective, mais lance surtout un défi au lecteur : qu’est-ce que la question ? Celle sociale de l’être lorsque l’humain s’apprête à marcher vers le cosmos, quelle est-elle ?


## Sur le collectif

Des mondes de l’architecture, de la philosophie, des arts plastiques, de la musique, de l’histoire, de l’anthropologie et de la mathématique, s’est formé un collectif quelque peu éclectique. Il se caractérise lui-même comme les esprits réunis d’une dimension renouvelée, apparue hasardeusement dans les ruines d’un réacteur nucléaire. Au travers de ses travaux, ce collectif se donne pour objectif principiel de reconstituer l’espace autour de la question délaissée de l’être. L’espace aurait une essence, mouvante et sociale, transformable et croissante, et à l’aurore d’une accélération de notre histoire, Dimension Ruine souhaite nous rappeler à son bon souvenir.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d’informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
